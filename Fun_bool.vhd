library ieee;
use ieee.std_logic_1164.all;

entity Fun_bool is
	port
	(
		-- Input ports
		puerta	: in  std_logic;
		motor		: in  std_logic;
		luces		: in  std_logic;
		-- Output ports
		alarma	: out std_logic
	);
end Fun_bool;


architecture behavioral of Fun_bool is

	-- Declarations (optional)

begin
	
	alarma <= (not motor and luces) or (puerta and motor);

	-- Process Statement (optional)

	-- Concurrent Procedure Call (optional)

	-- Concurrent Signal Assignment (optional)

	-- Conditional Signal Assignment (optional)

	-- Selected Signal Assignment (optional)

	-- Component Instantiation Statement (optional)

	-- Generate Statement (optional)

end behavioral;
